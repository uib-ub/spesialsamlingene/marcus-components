# Tests

Proof of concept, and tool to help with comparing to old marcus.

The tests check for a sample page per type in the sparql endpoint. To compare with old marcus, use marcus.xml, and save the result from oxygen into "save as xml" and do a diff.

Tresholds for differences and storing the results would benefit from having acceptable values (old marcus?) and setting a treshold for what should throw an error.

