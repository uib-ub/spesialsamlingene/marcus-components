<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:array="http://www.w3.org/2005/xpath-functions/array"
    xmlns:http="http://expath.org/ns/http-client"
    exclude-result-prefixes="xs"
    version="3.0">
    
       <xsl:include href="sparql.xsl"/>     
    
    
    <xsl:param name="hostname" select="'https://marcus.test.uib.no'"/>
    
    <xsl:param name="types" select="['http://data.ub.uib.no/ontology/Album']" as="array(xs:string)*"/>
    <xsl:output method="text" indent="yes"/>
    <xsl:variable name="type-label-exampleSubject-sparql">
        <xsl:text>
        PREFIX rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#&gt;    
        SELECT distinct ?type (sample(?s) AS ?subject) (sample(?label) AS ?typeLabel)
        WHERE {    
                {SELECT distinct ?type (sample (?o) AS ?or) WHERE { GRAPH ?G { ?s a ?type. }}
            GROUP by ?type
        }
        ?type rdfs:label ?label.
        ?s a ?type
        FILTER (STRLEN(STR(?label)) > 0)
        FILTER (!(CONTAINS(STR(?s),"rdf-xml")))         
        }
        GROUP by ?type
        </xsl:text>
    </xsl:variable>
    
    <xsl:variable name="other-slugs" select="['/home','/collections','/events','/albums','/exhibitions']"/>
    <xsl:template  name="xsl:initial-template">
        <xsl:message select="count(flub:sparqlSelect($type-label-exampleSubject-sparql,$sparql-endpoint)//*:result/child::*)"></xsl:message>
        <xsl:sequence select="flub:test-types(flub:sparqlSelect($type-label-exampleSubject-sparql,$sparql-endpoint)//*:result)"/>
    </xsl:template>
    
    <xsl:function name="flub:test-types">
        <xsl:param name="input" as="element(*)*"/>        
        
        <xsl:map>
        <xsl:iterate select="$input">
            <xsl:param name="results" as="map(*)" select="map{}"/>
           
            <xsl:on-completion>
                <xsl:iterate select="map:keys($results)">
                    <xsl:param name="failed" select="false()"/>
                <xsl:on-completion>
                    <xsl:sequence select="if ($failed) then error((),'Failed test') else ()"/>
                </xsl:on-completion>
                <xsl:variable name="type-key" select="."/>
                    <xsl:message select="'type: ' || $type-key || ' status:' || $results($type-key)('status') || 'url: ' || $results($type-key)('url') || ' time millis: ' || $results($type-key)('spent-millis') "/>
                    <xsl:sequence select="map { 'type' : $type-key, 'status' : $results($type-key)('status'), 'url' : $results($type-key)('url'), 'time_millis': $results($type-key)('spent-millis') }"></xsl:sequence>
                <xsl:choose>
                    <xsl:when test="$results($type-key)('status') !='200'">
                        <xsl:next-iteration>
                            <xsl:with-param name="failed" select="true()"/>
                        </xsl:next-iteration>
                    </xsl:when>
                </xsl:choose>
                </xsl:iterate>
                
            </xsl:on-completion>
            <xsl:variable name="result" select="self::node()" as="element(*)"/>
            <xsl:variable name="type" select="$result/*:binding[@name='type']" as="element(*)"/>
            <xsl:variable name="subject" select="$result/*:binding[@name='subject']" as="element(*)"/>
            <xsl:variable name="typeLabel" select="$result/*:binding[@name='typeLabel']" as="element(*)"/>
            
            <xsl:if test="array:size($types)=0 or array:size(array:filter($types,function($x){ some $y in $type//*:uri satisfies string($x) = string($y)})) > 0" >         
           
            <xsl:variable name="visit-uri" select="normalize-space(replace($subject,'http://data.ub.uib.no',$hostname))" as="xs:string"/>            
            
            <xsl:variable name="web-request" as="element(http:request)">
                <http:request method="GET"  follow-redirect="true">
                    <http:header name="accept" value="text/html"/>
                </http:request>
            </xsl:variable> 
            
            <xsl:variable name="web-result" select="if (contains($subject,'http://data.ub.uib.no')) then http:send-request($web-request,$visit-uri) else ()" />
                <xsl:next-iteration>

                    <xsl:with-param name="results"
                        select="
                            map:put($results, $type, map {
                                'url': $visit-uri,
                                'label': $typeLabel,
                                'status': string($web-result//@status),
                                'spent-millis': string($web-result//@spent-millis)
                            })"
                    />
                </xsl:next-iteration>
            </xsl:if>
        </xsl:iterate>
        </xsl:map>
    </xsl:function>
    
    <xsl:function name="flub:test">
        <xsl:param name="x"/>
        <xsl:param name="typelabel"/>
        <xsl:sequence select="if ($x = $typelabel) then $x else ()"></xsl:sequence>
    </xsl:function>
</xsl:stylesheet>