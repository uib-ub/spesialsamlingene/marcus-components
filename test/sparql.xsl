<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"
    xmlns:sparql-results="http://www.w3.org/2005/sparql-results#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    exclude-result-prefixes="xs"
    version="3.0">
    
    <!-- sjekker etter innehold i de forskjellige conceptSchemes.
        Gir match dersom det finnes kun en match på label innenfor conceptScheme.
        Prioritert rekkefølge på stedsnavn er da marcus-register (billed) (uten conceptscheme og http://data.ub.uib.no/conceptscheme/billedsamlingens-emneord), så stedsregisteret til manlib, så diplomsamlingen (http://data.ub.uib.no/conceptscheme/6c5662da-cfab-4c32-8a91-dc524ca233e9) -->
    <!-- diplomsamlingen 
        http://data.ub.uib.no/conceptscheme/6c5662da-cfab-4c32-8a91-dc524ca233e9
    -->
    <xsl:variable name="billedsamlingens-emneord" select="'http://data.ub.uib.no/conceptscheme/billedsamlingens-emneord'"/>
    <xsl:variable name="ms-lib-register" select="'http://data.ub.uib.no/conceptscheme/8c7a6025-d771-4d6c-9c41-72f5b36ecde5'"/>
    <xsl:variable name="diplomsamlingens-register" select="'http://data.ub.uib.no/conceptscheme/6c5662da-cfab-4c32-8a91-dc524ca233e9'"/> 
    
    <xsl:variable name="escapechars-regex">
        <xsl:text>(["\\])</xsl:text>
    </xsl:variable>
    <!-- http://marcus.uib.no/sparql/marcus-prod/sparql?query=-->
    <xsl:param name="sparql-endpoint" select="'http://sparql.ub.uib.no/sparql?query='"/>
    
    <xsl:function name="flub:lookupSpatialThing">
        <xsl:param name="place-label"/>
       
        <xsl:variable name="place-label" select="replace($place-label,$escapechars-regex,'\$1')"/>
        <xsl:variable name="sparql-query">
            PREFIX rdf:&lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#>                 
            PREFIX ubbont: &lt;http://data.ub.uib.no/ontology/> 
            PREFIX geo: &lt;http://www.w3.org/2003/01/geo/wgs84_pos#>
            PREFIX skos: &lt;http://www.w3.org/2004/02/skos/core#>
                        construct {?s ?p ?o} 
                        WHERE {GRAPH ?g {?s a geo:SpatialThing.
                        ?s skos:prefLabel "<xsl:value-of select="normalize-space($place-label)"/>".
                        ?s ?p ?o.
                        OPTIONAL { MINUS {?s ubbont:locationFor ?location.} }
                        }}                        
        </xsl:variable>
        <xsl:variable name="xml-output" select="'&amp;output=xml'"/>
        <xsl:variable name="result" select="document(concat($sparql-endpoint,encode-for-uri($sparql-query),$xml-output))"/>
        <!--henter resultater for hvert register-->
        <xsl:variable name="billed-results" select="$result/rdf:RDF/*[skos:inScheme/@rdf:resource=$billedsamlingens-emneord or not(skos:inScheme)]"/>
        <xsl:variable name="manlib-results" select="$result/rdf:RDF/*[skos:inScheme/@rdf:resource=$ms-lib-register]"/>
        <xsl:variable name="diplom-results" select="$result//rdf:RDF/*[skos:inScheme/@rdf:resource=$diplomsamlingens-register]"/>
        
        <xsl:choose>
            <xsl:when test="count($result/descendant-or-self::rdf:RDF/*[skos:inScheme=$billedsamlingens-emneord or not(skos:inScheme)])=1">
                <xsl:value-of select="$billed-results/@rdf:about"/>
            </xsl:when>
            <xsl:when test="count($billed-results) = 0 and count($manlib-results)=1">
                <xsl:value-of select="$manlib-results/@rdf:about"/>
            </xsl:when>
            <xsl:when test="count($billed-results) = 0 and count($manlib-results)=0 and count($diplom-results)=1">
                <xsl:value-of select="$diplom-results/@rdf:about"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:choose>               
                <xsl:when test="$result/rdf:RDF/*">
                    <xsl:message>Flere steder med samme navn for label  <xsl:value-of select="$place-label"/> <xsl:text>Sparql resultat er</xsl:text><xsl:value-of select="$result"/>       </xsl:message>          
                </xsl:when>
                <xsl:otherwise>
                    <xsl:message>Stedsnavn ikke funnet. <xsl:value-of select="$place-label"/></xsl:message>
                </xsl:otherwise>
               </xsl:choose>
                <xsl:value-of select="$place-label"/>                
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:function>
    
    <xsl:function name="flub:sparqlSelect" as="document-node(element(sparql-results:sparql))">
        <xsl:param name="query" as="xs:string"/>
        <xsl:param name="endpoint" as="xs:string"/>
        <xsl:sequence select="flub:sparqlResults($endpoint,$query)"/>
    </xsl:function>
    
    <xsl:function name="flub:sparqlResults" as="document-node(element(*))">
        <xsl:param name="endpoint" as="xs:string"/>
        <xsl:param name="query" as="xs:string"/>
        <xsl:sequence select="document(concat($endpoint,encode-for-uri($query),'&amp;output=xml'))"/>
    </xsl:function>
    
    <xsl:function name="flub:sparqlQuery" as="document-node(element(rdf:RDF))">
        <xsl:param name="query" as="xs:string"/>
        <xsl:param name="endpoint"/>
        <xsl:if test="string($query)">
        <xsl:sequence select="document(concat($endpoint,encode-for-uri($query),'&amp;output=xml'))"/>
        </xsl:if>
    </xsl:function>
    
    <!--  one param version with default endpoint-->
    <xsl:function name="flub:sparqlQuery" as="document-node(element(rdf:RDF))">
        <xsl:param name="query" as="xs:string"/>
        <xsl:sequence select="flub:sparqlQuery($query,$sparql-endpoint)"/>
        
    </xsl:function>
    <xsl:function name="flub:lookupPlaceInDiplom">
        <xsl:param name="place-label"/>
        
        <xsl:variable name="place-label" select="replace($place-label,$escapechars-regex,'\$1')"/>
        <xsl:variable name="sparql-query">
            PREFIX rdf:&lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#>                 
            PREFIX ubbont: &lt;http://data.ub.uib.no/ontology/> 
            PREFIX geo: &lt;http://www.w3.org/2003/01/geo/wgs84_pos#>
            PREFIX skos: &lt;http://www.w3.org/2004/02/skos/core#>
            construct {?s ?p ?o} 
            WHERE {GRAPH ?g {?s a geo:SpatialThing.
            ?s skos:prefLabel "<xsl:value-of select="normalize-space($place-label)"/>"@no.
            ?s ?p ?o.
            OPTIONAL { MINUS {?s ubbont:locationFor ?location.} }
            }}                        
        </xsl:variable>
        <xsl:variable name="xml-output" select="'&amp;output=xml'"/>
        <xsl:variable name="result" select="document(concat($sparql-endpoint,encode-for-uri($sparql-query),$xml-output))"/>
        <!--henter resultater for hvert register-->
        <xsl:variable name="diplom-results" select="$result//rdf:RDF/*[skos:inScheme/@rdf:resource='http://data.ub.uib.no/instance/concept/diplomsamlingens-stedsregister']"/>
        
        <xsl:choose>                   
            <xsl:when test="count($diplom-results)=1">
                <xsl:sequence select="$diplom-results"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>               
                    <xsl:when test="$result/rdf:RDF/*">
                        <xsl:message>Flere steder med samme navn for label  <xsl:value-of select="$place-label"/> <xsl:text>Sparql resultat er</xsl:text><xsl:value-of select="$result"/>       </xsl:message>          
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:message>Stedsnavn ikke funnet. <xsl:value-of select="$place-label"/></xsl:message>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:value-of select="$place-label"/>                
            </xsl:otherwise>
        </xsl:choose>        
    </xsl:function>
    
    <xsl:function name="flub:chunk-sparql-query" as="element(rdf:RDF)*">
        <xsl:param name="query"/>
        <xsl:variable name="results" select="flub:chunk-sparql-query($query,0,2000)" as="element(rdf:RDF)*"/>
        
       
            <xsl:iterate select="$results">
            <xsl:copy>   
                <xsl:copy-of select="@*"/>
                 <xsl:sequence select="$results/descendant-or-self::rdf:RDF/*"/>
            </xsl:copy>
                <xsl:break/>
            </xsl:iterate>            
            </xsl:function>
    
    <xsl:function name="flub:chunk-sparql-query" as="element(rdf:RDF)*">
        <xsl:param name="query"/>
        <xsl:param name="offset" as="xs:integer"/>
        <xsl:param name="limit" as="xs:integer"/>        

        <xsl:variable name="limited-query">
            <xsl:text expand-text="1">{$query} LIMIT {$limit} OFFSET {$offset}</xsl:text>
        </xsl:variable>
        
        <xsl:variable name="sparql-result" select="flub:sparqlQuery($limited-query)"/>
        
        <xsl:choose>
            <xsl:when test="$sparql-result/rdf:RDF/*">
                <xsl:sequence select="$sparql-result/rdf:RDF"/>
                <xsl:sequence select="flub:chunk-sparql-query($query,$offset+$limit,$limit)"/>                
            </xsl:when>
        </xsl:choose>
    </xsl:function>
</xsl:stylesheet>