<div class="row">
	<div class="col-md-12">
	<div class="alert alert-info" role="alert">
		<h5 style="font-weight: bolder;">Bruksvilkår</h5>
		  <p>Spesialsamlingene tilbyr digitale filer av materialet som finnes i samlingene. Hvis ikke annet er spesielt avtalt gjelder følgende regler:</p>

<ol style="list-style-position: inside; padding-left: 0; margin-top: 0.5em">
<li>Alle bilder fra Spesialsamlingene/Billedsamlingens nettsted kan fritt lastes ned i den form de er på Marcus og kan brukes, deles, kopieres og distribueres gratis, mot kreditering.</li>
<li>Ved all publisering, både digitalt og fysisk, skal det alltid oppgis fotograf (hvis kjent) og eierinstitusjon (Universitetsbiblioteket i Bergen).</li>
<li>Bruker skal vise aktsomhet ved gjengivelse av bilder, slik at det ikke skjer til skade for / krenker avbildede personer eller skader fotografens ry (jfr. Åndsverksloven).</li>
<li>Vær oppmerksom på at bruker selv er ansvarlig for å avklare eventuelle rettigheter knyttet til personvern (jfr. Personopplysningsloven).</li>
</ol>
<p>Profesjonelle brukere, forskere og studenter kan kontakte Spesialsamlingene ved andre behov.
      {% if "manlib" in first.digitalresources.imgTH.value %}
      <a href="https://www.uib.no/ub/108555/åpningstider-og-kontaktinformasjon#manuskript-og-librarsamlingen" target="_blank">Kontakt Manuskript- og librarsamlingen</a>
      {% else %}
      <a href="https://www.uib.no/ub/108555/%C3%A5pningstider-og-kontaktinformasjon#billedsamlingen" target="_blank">Kontakt Billedsamlingen</a>. Billedsamlingen kan, etter forespørsel, digitalisere fra samlingene. Dette kan medføre en kostnad. <a href="https://www.uib.no/ub/spesialsamlingene" target="_blank">På nettsidene våre kan du lese mer om hva vi kan hjelpe med.</a>
      {% endif %}
</p>
</div>
    </div>
</div>
