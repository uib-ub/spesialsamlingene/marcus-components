            <div id="dzi1"></div>
          {% set  osd_path = lodspk.home . 'js/bower_components/openseadragon/bower_components/openseadragon/built-openseadragon/openseadragon/' %} 
            <!-- <div id="navigatorDiv"></div> -->
             <div class="hidden-md hidden-lg hidden-xs hidden-sm" id="dzi2"></div>
			 <div id="dzi-control" class="row">
 			 	<ul class="list-inline">
					<li id="resetZoom"><a class="btn btn-default"><i class="fa fa-arrows-alt"></i> <span class="hidden-xs">tilpass zoom</span></a></li>
					<li id="full-page"><a class="btn btn-default" alt="Fullskjermvisning av og på"><i id="fullPage"></i></a></li>
					<li><i class="text-muted fa fa-minus"></i></li>
					<li id="zoom-out"><button class="btn btn-default"><i class="fa fa-search-minus"></i></button></li>
					<li id="zoom-in"><button class="btn btn-default"><i class="fa fa-search-plus"></i></button></li>
					{% if models.dzi|length > 1 %}
					<li id="previous"><a class="btn btn-default"><i class="fa fa-arrow-left"></i> forrige</a></li>
					<li>gå til side &nbsp;<input style="width: 55px;" type="number" id="page" value="1"/> av <span id="tileSourcesLength"></span></li>
					<li id="next"><a class="btn btn-default">neste <i class="fa fa-arrow-right"></i></a></li>
					{%endif%}
				</ul>
			</div>
			<!-- <div id="navigatorDiv" style="width: 100px; height: 100px;"></div> -->
    <script type="text/javascript" src="{{osd_path}}openseadragon.js"></script>
		
    <script type="text/javascript"> 
	    var viewer = OpenSeadragon({
	      id:            "dzi1",
        prefixUrl:     "{{osd_path}}/images/",
				tileSources:   [     
				{% for row in models.dzi %}{%if !forloop.first && models.dzi|length > 1 %},{%endif%}
				"{{ row.dziUri.value }}"{% endfor %}
				],
	      //visibilityRatio: 0.3,
	      //minZoomLevel: 0.7,
	      constrainDuringPan: false,
	      preserveViewport: false,
				sequenceMode: true,
				//minZoomImageRatio: 0.6,
	      minPixelRatio: 0.5,
	      animationTime: 0.8,
	      springStiffness: 8,
	      showNavigator:  true,
				//navigatorId:   "navigatorDiv",
	      toolbar:       "dzi-control",
	      zoomInButton: "zoom-in",
	      zoomOutButton:  "zoom-out",
		    homeButton:     "resetZoom",
		    fullPageButton: "full-page",
		    nextButton:     "next",
		    previousButton: "previous"
	    });
	    $('#page').change(function() {
	    	chpage = $('#page').val();
	    	currentPage = chpage-1;
	    	viewer.goToPage(chpage-1); 
	    	});
	    viewer.addHandler('page', function(event){
	    	$('#page').val(event.page+1);
	    	});
		function tileSourceLength() {
	    	length = viewer.tileSources.length ;
	    	$('#tileSourcesLength').append(length);
	    	};
	    tileSourceLength();
	    // Under forsoek paa aa lage flip paa ikon for aa gaa inn og ut av fullskjermvisning
	    function fullPage() {
	    	if (viewer.fullScreen === true) {
		    	$('#fullPage').removeClass('fa fa-expand').addClass('fa fa-compress');
	    	}
	    	else {
		    	$('#fullPage').removeClass('fa fa-compress').addClass('fa fa-expand');
	    	}
	    	};
	    fullPage();
    </script>
