<footer class="footer hidden-print">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="col-md-12 hidden-xs">
					<img class="col-md-12" data-pin-nopin="true" src="{{lodspk.home}}img/UiBmerke_grayscale.svg" />
				</div>
				
				<p class="text-center"><span class="marcus-logo">Marcus</span> er Spesialsamlingene til <a href="https://www.uib.no/ub">Universitetsbiblioteket i Bergen</a> sin portal til digitaliserte manuskript, fotografi, diplomer og mye mer. Oppkalt etter <a href="/instance/person/f37c8581-29b2-4724-8900-965223b3cf06">Marcus Selmer</a>, Bergens første fotograf.</p>
			</div>
			<div class="col-md-3">
<!--
				<ul class="nav">
					<li><a href="{{lodspk.home}}collections"><i style="color: black;" class="fa fa-th fa-fw"></i>  Samlinger</a></li>
					<li><a href="{{lodspk.home}}topics"><i style="color: black;" class="fa fa-tags fa-fw"></i>  Emner</a></li>
					<li><a href="{{lodspk.home}}persons"><i style="color: black;" class="fa fa-users fa-fw"></i>  Personer</a></li>
					<li><a href="{{lodspk.home}}conceptschemes"><i style="color: black;" class="fa fa-sitemap"></i></i>  Register</a></li>
					<li><a href="{{lodspk.home}}manuscripts"><i class="fa fa-book"></i>  Manuskript</a></li>
					<li><a href="{{lodspk.home}}charters"><i class="fa fa-book"></i>  Diplom</a></li>
				</ul>
-->
				<ul class="list-unstyled">
					<li class="h5"><i class="fa fa-info fa-lg fa-fw"></i> <a href="{{lodspk.home}}about-marcus">Om nettsiden</a></li>
					<li class="h5"><i class="fa fa-universal-access fa-lg fa-fw"></i> <a href="https://uustatus.no/nb/erklaringer/publisert/db9c1b9e-b289-4668-9bfb-a851904f292e">Tilgjengelighetserklæring</a></li>
					<li class="h5"><i class="fa fa-cubes fa-lg fa-fw"></i> <a href="https://sparql.ub.uib.no">Åpne data (Sparql)</a></li>
					<li class="h5"><i class="fa fa-cog fa-lg fa-fw"></i> <a href="{{lodspk.home}}technology.html">Teknologi</a></li>
					<li class="h5"><i class="fa fa-bar-chart-o fa-lg fa-fw"></i> <a href="{{lodspk.home}}stats">Statistikk</a></li>
				</ul>
			</div>	
			
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-6 col-sm-6">
						<h4>Manuskript- og librarsamlingen</h4>
						<p><i class="fa fa-clock-o"></i> <a href="https://www.uib.no/ub/108555/%C3%A5pningstider-og-kontaktinformasjon#manuskript-og-librarsamlingen">Åpningstider og kontaktinformasjon</a></p>
						<address>
						<i class="fa fa-envelope"></i> <a href="mailto:mslib@uib.no">Epost</a>
						</address>
					</div>
		
					<div class="col-md-6 col-sm-6">
						<h4>Billedsamlingen</h4>
						<p><i class="fa fa-clock-o"></i> <a href="https://www.uib.no/ub/108555/%C3%A5pningstider-og-kontaktinformasjon#billedsamlingen">Åpningstider og kontaktinformasjon</a></p>

                                                <address>
						<i class="fa fa-envelope"></i> <a href="mailto:billed@uib.no">Epost</a>
						</address>
						<p><i class="fa fa-facebook-square"></i> <a href="https://www.facebook.com/pages/Billedsamlingen/132221843465746">Følg Billedsamlingen på Facebook</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<hr style="border: 1px solid #ddd;">
			<div class="col-md-12">
				<div typeof="dcat:Dataset" resource="http://data.ub.uib.no/dataset/marcus">
					<p>
						<a rel="dct:license" href="http://creativecommons.org/publicdomain/zero/1.0/">
							<img src="/img/cc.large.png" style="border-style: none;" width="20px" alt="CC0" />
							<img src="/img/zero.large.png" style="border-style: none;" width="20px" alt="CC0" />
						</a> 
						<small property="dct:rights"><span rel="dct:publisher" resource="http://data.ub.uib.no/instance/organization/universitetsbiblioteket-i-bergen">Universitetsbiblioteket i Bergen</span> deler <span property="dct:title">Marcus datasettet</span> fritt.</small><br>
						<small>Innholdet i Marcus er derimot ikke delt med CC0. Innhold vil bli merket med korrekt lisens.</small>
					</p>
				</div>			
				<p><small>Feil på sidene? Kontakt <a href="maito:tarje.lavik@uib.no">webredaktør</a>.</small></p>
			</div>
		</div>
	</div>
</footer>  
  
<script type="text/javascript">
$('.bs-sidebar').affix({
    offset: {
      top: 120,
      offset: { top: 120 },
      bottom: function () {
        return (this.bottom = $('footer').outerHeight(false)-300)
      }
    }
  })
  </script>
  
    <script src="{{lodspk.home}}js/holder.js"></script>
    
    <!-- Piwik -->
	<script type="text/javascript">
	  var _paq = _paq || [];
	  _paq.push(["trackPageView"]);
	  _paq.push(["enableLinkTracking"]);
	
	  (function() {
	    var u=(("https:" == document.location.protocol) ? "https" : "http") + "://stats.uib.no/";
	    _paq.push(["setTrackerUrl", u+"piwik.php"]);
	    _paq.push(["setSiteId", "23"]);
	    var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
	    g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
	  })();
	</script>
	<!-- End Piwik Code -->
