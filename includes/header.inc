<!DOCTYPE html>
<html lang="no" prefix="og: http://ogp.me/ns# 
	dct: http://purl.org/dc/terms/ 
	dcat: http://www.w3.org/ns/dcat#">
 
  <head>
    <meta charset="utf-8">
    <title>{% if first.main.title.value != "" %}{{first.main.title.value}} - {% else %}{% if first.main.label.value != "" %}{{first.main.label.value}} - {%endif%}{%endif%}{{lodspk.title}}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" type="image/x-icon" href="{{lodspk.home}}/img/favicon.ico">
    
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-72707758-1', 'auto');
      ga('send', 'pageview');

  </script>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- <script type="text/javascript" src="{{lodspk.home}}js/jquery.js"></script> -->
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	
	<!-- Latest compiled and minified JavaScript -->
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

	<link href="{{lodspk.home}}css/basic.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="{{lodspk.home}}css/print.css" rel="stylesheet" type="text/css" media="print" />
    
    <!-- Font Awesome -->
    <!-- <link rel="stylesheet" href="{{lodspk.home}}static/Font-Awesome/css/font-awesome.min.css"> -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- Leaflet CSS -->
    <link rel="stylesheet" href="{{lodspk.home}}js/Leaflet/leaflet.css" />
	<!--[if lte IE 8]>
	    <link rel="stylesheet" href="{{lodspk.home}}/js/Leaflet/leaflet.ie.css" />
	<![endif]-->
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{% if first.main.description.value %}{{first.main.description.value}}{% else %}{% if first.description.description.value %}{{first.description.description.value}}{% else %}Marcus er Spesialsamlingene til Universitetsbiblioteket i Bergen sin portal til digitaliserte manuskript, fotografi, diplomer og mye mer. Oppkalt etter Marcus Selmer, Bergens første fotograf.{% endif %}{% endif %}">
    <meta name="author" content="{{first.maker.makerName.value}}">
    
    <link rel="canonical" href="{{lodspk.local.value}}">

    
    <!-- FACEBOOK INSIGHT -->
    <meta property="fb:admins" content="717130125" />
    
    <!-- FACEBOOK OPEN GRAPH -->
    <meta property="og:type" content="{%if lodspk.local.value == "https://marcus.uib.no/home" %}website{%else%}article{%endif%}"/>
    <meta property="og:title" content="{% if first.main.title != "" %}{{first.main.title.value}}{% else %}{% if first.main.label != "" %}{{first.main.label.value}}{%endif%}{%endif%}"/>
    <meta property="og:description" content="{% if first.main.description.value %}{{first.main.description.value}}{% else %}{% if first.description.description.value %}{{first.description.description.value}}{% else %}Marcus er Spesialsamlingene til Universitetsbiblioteket i Bergen sin portal til digitaliserte manuskript, fotografi, diplomer og mye mer. Oppkalt etter Marcus Selmer, Bergens første fotograf.{% endif %}{% endif %}"/>
    <meta property="og:image" content="{%if first.digitalresources.imgMD %}{{first.digitalresources.imgMD.value}}{% else %}{{first.digitalresources.imgSM.value}}{% endif %}"/>
    <meta property="og:image" content="{%if main.logo %}{{main.logo.value}}{% endif %}"/>
    <meta property="og:url" content="{{lodspk.local.value}}"/>
    <meta property="og:site_name" content="Marcus"/>
    
    <!-- TWITTER CARD -->

	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:url" content="{{lodspk.local.value}}">
	<meta name="twitter:description" content="{% if first.description.description.value %}{{first.description.description.value}}{% else %}Marcus er Spesialsamlingene til Universitetsbiblioteket i Bergen sin portal til digitaliserte manuskript, fotografi, diplomer og mye mer. Oppkalt etter Marcus Selmer, Bergens første fotograf.{% endif %}">
	<meta name="twitter:image:src" content="{%if first.digitalresources.imgMD %}{{first.digitalresources.imgMD.value}}{% else %}{{first.digitalresources.imgSM.value}}{% endif %}">
	<meta name="twitter:domain" content="marcus.uib.no">
	<meta name="twitter:site" content="@UiB_UB">
	<meta name="twitter:creator" content="@UiB_UB">

	<!-- CITATION -->
	<meta name="citation_title" content="{% if first.main.title != "" %}{{first.main.title.value}}{% else %}{% if first.main.label != "" %}{{first.main.label.value}}{%endif%}{%endif%}">
	<meta name="citation_publisher" content="Avdeling for spesialsamlinger, Universitetsbiblioteket i Bergen">
	{% spaceless %}
	{% if not first.maker.makerName.value | null %}
		{% for row in models.maker %}
			{% ifchanged row.maker.value %}
	<meta name="dc.creator" content="{{ row.makerName.value }}" />
			{% endifchanged %}
		{% endfor %}
	{% endif %}
	{% endspaceless %}

	
	<script type="text/javascript" src="{{lodspk.home}}js/qrcode/jquery.qrcode-0.7.0.min.js"></script>
	
	<link href='https://fonts.googleapis.com/css?family=IM+Fell+Great+Primer' rel='stylesheet' type='text/css'>

    <link rel="alternate" type="application/rdf+xml" title="RDF/XML Version" href="{{lodspk.local.value}}.rdf" />
    <link rel="alternate" type="text/turtle" title="Turtle Version" href="{{lodspk.local.value}}.ttl" />
    <link rel="alternate" type="text/plain" title="N-Triples Version" href="{{lodspk.local.value}}.nt" />
    <link rel="alternate" type="application/json" title="RDFJSON Version" href="{{lodspk.local.value}}.json" />
    
    <script type="text/javascript" src="{{lodspk.home}}js/bootstrap-typeahead.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $('.typeahead').typeahead({
            minLength: 4,
            source: function (typeahead, query) {
              $('.typeahead').addClass('wait');[]
              return $.get('{{lodspk.home}}search/'+encodeURIComponent(query), { }, function (data) {
                  $('.typeahead').removeClass('wait');[]
                  return typeahead.process(data);
              }, 'json');
            },
            onselect: function (obj) {
              $('.typeahead').attr('disabled', true);
              window.location = obj.uri;
            }
        });
        $(".header-search").on('keyup', function(){$("form").attr("action", '{{lodspk.home}}search/'+$(this).val())});
    });
    </script>
    
	<script src="{{lodspk.home}}/js/Leaflet/leaflet.js"></script>
	<script src="{{lodspk.home}}js/Leaflet/leaflet-providers.js"></script>
	
  </head>
  <body>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '332990033526856',
      xfbml      : true,
      version    : 'v2.5'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
	  
	  
      <div id="wrap">
	    <header>
			<nav style="margin-bottom: 0;" class="navbar navbar-inverse navbar-static-top marcus-header" role="navigation">  
				<div class="container-fluid">
					<div class="navbar-header">
			            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			              <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
			            </button>
			            <img class="logo" style="float: left;" data-pin-nopin="true" src="{{lodspk.home}}img/UiBmerke_white.svg">
			            <a class="navbar-brand" href="{{lodspk.home}}">{{lodspk.title}}</a>
			            <a class="nav-subtitle hidden-xs hidden-sm hidden-md" href="{{lodspk.home}}"><small>Spesialsamlingene ved <br>Universitetsbiblioteket i Bergen</small></a>
		        	</div>
		        	
        <div class="collapse navbar-collapse">
          <form class="navbar-form pull-right" role="search" method="get" action="/search">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Søk ..." name="q">
							<span class="input-group-btn">
				
								<button type="submit" class="btn btn-default">
									<i class="fa fa-search fa-lg">
										<span class="sr-only">Search</span>
									</i>
								</button>
							</span>
						</div>
					</form>
					
	       
	     <ul class="nav navbar-nav navbar-right">
						 <li><a href="{{lodspk.home}}">Hjem</a></li>

		              	<li class="dropdown">
						  	<a id="browse1" class="dropdown-toggle" role="button" data-toggle="dropdown" data-target="#" href="#"> Meny <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
						  	<ul class="dropdown-menu" aria-labelledby="browse1" role="meny">
							

								
				<li><a href="{{lodspk.home}}collections"><i style="color: #aaa;" class="fa fa-th fa-fw"></i>  Samlinger</a></li>
				<li><a href="{{lodspk.home}}search/?filter=type.exact%23Fotografi&filter=type.exact%23Album&filter=type.exact%23Grafikk&filter=type.exact%23Postkort"><i style="color: #aaa;" class="fa fa-picture-o fa-fw"></i>  Bilder</a></li>
				<li><a href="{{lodspk.home}}events"><i style="color: #aaa;" class="fa fa-calendar fa-fw"></i>  Hendelser</a></li>
				<li><a href="{{lodspk.home}}albums"><i style="color: #aaa;" class="fa fa-book fa-fw"></i>  Album</a></li>
				<li><a href="{{lodspk.home}}exhibitions"><i style="color: #aaa;" class="fa fa-university fa-fw"></i>  Utstillinger</a></li></ul>
						</li>
						<li class="dropdown">
							<a id="browse1" class="dropdown-toggle" role="button" data-toggle="dropdown" data-target="#" href="#"> Om <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
							<ul class="dropdown-menu" aria-labelledby="browse2" role="meny">
								<li><a href="{{lodspk.home}}about-marcus"><i class="fa fa-archive"></i> Om Marcus</a></li>
								<li><a href="https://sparql.ub.uib.no"><i class="fa fa-cubes"></i> Åpne data (Sparql)</a></li>
							</ul>
						</li>						
     
					</ul>
				</div>
	
			</div>
	    </nav><!--/.nav-collapse -->
      </header>  
	      
