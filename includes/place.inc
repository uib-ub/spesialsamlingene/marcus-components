		<div class="map-wrap panel panel-default">			
			{% if first.main.hasLong.value == "false" %}
			<div class="panel-heading">
	    		<h3 class="panel-title"><i class="fa fa-map-marker"></i> Tilknyttede steder</h3>
			</div>
			{% endif %}
			
			{% if first.main.hasLong.value == "true" %}
			<div id="map"></div>
			{% endif %}
			
			
			<div class="panel-body">		    		
	    		<ul class="places-inline-list">
  				{% for row in models.places %}
  				{% ifchanged row.uri.value %}
  		        	<li><i class="fa fa-map-marker"></i> <a href="{{ row.uri.value }}">{{row.label.value}}</a></li>
  		        {% endifchanged %}
  				{% endfor %} 
  				</ul>
			</div>
			
		</div>
		
		<script>			
			var map = L.map('map', {maxZoom: 18});
			var bounds = [{% for row in models.places %}{%if !forloop.last && not row.lat.value | null%}, {%endif%}{% if not row.lat.value | null %}[{{ row.lat.value }}, {%endif%}{% if row.long.value != "" %}{{ row.long.value }}]{%endif%}{% endfor %}];
			bounds = $.grep(bounds,function(n){ return(n) });
			if (bounds.length > 1) { map.fitBounds(new L.LatLngBounds(bounds)); } else { map.setView(bounds[0], 12);}
			var defaultLayer = L.tileLayer.provider('OpenStreetMap.Mapnik').addTo(map);
			var baseLayers = [ 'OpenStreetMap.Mapnik', 'MapQuestOpen.OSM', 'MapQuestOpen.Aerial', 'Stamen.Watercolor' ];
			var overlayLayers = [ 'Kartverket', 'Rodekart9', 'Rodekart4', 'Rodekart1' ];
			L.control.layers.provided(baseLayers, overlayLayers, {collapsed: true}).addTo(map);
			{% for row in models.places %}{% if not row.long.value | null %}
	    	L.marker([{{ row.lat.value }}, {{ row.long.value }}]).addTo(map).bindPopup("<b><a href=\"{{ row.uri.value }}\">{{row.label.value}}</a></b>").openPopup();
	        {% endif %}{% endfor %} 
			map.touchZoom.disable();		
		</script>
