	<li><a href="{{lodspk.home}}collections"><i style="color: black;" class="fa fa-th fa-fw"></i>  Samlinger</a></li>
	<li><a href="{{lodspk.home}}search/?filter=type.exact%23Fotografi&filter=type.exact%23Album&filter=type.exact%23Grafikk&filter=type.exact%23Postkort"><i style="color: black;" class="fa fa-picture-o fa-fw"></i>  Bilder</a></li>
	<li class="hidden"><a href="{{lodspk.home}}topics"><i style="color: black;" class="fa fa-tags fa-fw"></i>  Emner</a></li>
	<li class="hidden"><a href="{{lodspk.home}}agents"><i style="color: black;" class="fa fa-users fa-fw"></i>  Aktører</a></li>
	<li><a href="{{lodspk.home}}events"><i style="color: black;" class="fa fa-calendar fa-fw"></i>  Hendelser</a></li>
	<li><a href="{{lodspk.home}}albums"><i style="color: black;" class="fa fa-book fa-fw"></i>  Album</a></li>
	<li><a href="{{lodspk.home}}exhibitions"><i style="color: black;" class="fa fa-university fa-fw"></i>  Utstillinger</a></li>