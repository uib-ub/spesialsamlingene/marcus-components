    {% if models.collections|length != 0  %}
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title"><i class="fa fa-archive"></i> Samling</h3>
		</div>
		<div class="panel-body">
	        {% for row in models.collections %}
	        	{% if row.logo %}
	        	<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
			        <div class="col-lg-4 col-md-4 col-sm-2 hidden-xs">
			        	<a href="{{row.uri.value}}"><img class="img-responsive img-border img-circle" src="{{row.logo.value}}"/></a>
			        </div>
			        <div class="col-lg-8 col-md-8 col-sm-10">
			        	<h4><a href="{{row.uri.value}}">{% if row.title && row.title.value != "" %}{{row.title.value}}{% else %}{% if row.label && row.label.value != "" %}{{row.label.value}}{% else %} {{row.identifier.value}}{%endif%}{%endif%}</a></h4>
			        	{% if row.description %}
			        	<p>{{row.description.value|truncatechars:65}} <a href="{{row.uri.value}}">se mer</a>{%endif%}</p>
			        </div>
				</div>
				{% endif %}
	        {% endfor %}
	        
	        {% for row in models.collections %}
	        	{% if !row.logo %}
	        	<div class="col-md-6 col-lg-6 col-sm-10 col-xs-10 col-sm-offset-2 col-lg-offset-0 col-md-offset-0">
	        	<h4><a href="{{row.uri.value}}">{% if row.title && row.title.value != "" %}{{row.title.value}}{% else %}{% if row.label && row.label.value != "" %}{{row.label.value}}{% else %} {{row.identifier.value}}{%endif%}{%endif%}</a></h4>
	        	{% if row.description %}
	        	<p class="text-muted">{{row.description.value|truncatewords:25}} <a href="{{row.uri.value}}">se mer</a>{%endif%}</p>
		    </div>
				{% endif %}
	        {% endfor %}
	        </div>
		</div>
    {% endif %} 
    
