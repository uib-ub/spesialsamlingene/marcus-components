<!-- PUBLISHED -->
		    {% for row in models.published %}
	    	<p><small>Publisert i</small><br><a href="{{row.uri.value}}">{{row.label.value}}</a> {% if row.vol %} vol. {{row.vol.value}} {% endif %}{% if row.issue %} nr. {{row.issue.value}} {% endif %} {% if row.pageStart && row.pageEnd %}(s. {{row.pageStart.value}}-{{row.pageEnd.value}}){% endif %}<br>{% if row.issn %}<small>ISSN: {{row.issn.value}}{% endif %}</small></p>
			{% endfor %}
			
			<!-- PUBLISHER -->
	    	{% for row in models.publisher %}
	    	<p><small>Utgitt av</small><br><a href="{{row.uri.value}}">{{row.label.value}}</a> {% if first.main.isbn %}<br><small>ISBN: {{first.main.isbn.value}}</small>{% endif %}</p>
			{% endfor %}
			
		    <!-- PHYSICAL INFORMATION -->
		    {% if models.physical|length != 0 %}
			<p><i class="fa fa-stethoscope"></i><a data-toggle="collapse" data-target="#physical-reference"> Fysisk beskrivelse <span class="caret"></span></a></p>
			  
			<ul id="physical-reference" class="collapse">
			{% if first.physical.height.value != null && first.physical.width.value != null %}
				<li><i class="fa fa-arrows-v"></i> {{ first.physical.height.value }}</li>
				<li><i class="fa fa-arrows-h"></i> {{ first.physical.width.value }}</li>
			{% endif %}
			{% if first.main.pages.value != null %}
				<li>{{ first.main.pages.value }} sider</li>
		    {% endif %}
		    {% if first.physical.physicalCondition.value != null %}	
				<li><i>{{ first.physical.physicalCondition.value }}</i></li>
		    {% endif %}
		    {% if first.physical.physicalDescription.value != null %}
				<li>{{ first.physical.physicalDescription.value }}</li>
		    {% endif %}
		    </ul>
		    {% endif %}
		    
		    <!-- SUBJECTS -->
			{% if models.subjects|length != 0 %}
			<!-- <p><small><i class="fa fa-tags"></i> Emne(r)</small></p>-->
			<hr>
				<ul class="list-inline">
					{% for row in models.subjects %}
					{%ifchanged row.uri.value%}
			    	<li class="label label-marcus-subject"><a href="{{ row.uri.value }}">{{ row.label.value }}</a></li>
			    	{%endifchanged%}
					{% endfor %} 
				</ul>
				<hr>
			{% endif %}
			
			<!-- IDENTIFIER -->
			{% if first.main.identifier %}
			<ul class="list-unstyled">
				<li><small><i data-trigger="hover" data-toggle="tooltip" data-placement="left" title="Dokumentets signatur" class="fa fa-key"></i> Signatur</small><br>
				<i class="fa fa-bookmark"></i> <a href="{{lodspk.local.curie}}">{{ first.main.identifier.value }}</a></li>
		    <script>$('[data-toggle="tooltip"]').tooltip({'placement': 'top'});</script>
		    {% endif %}
		    
			<hr>
			
			<!-- DISQUS COMMENTS -->
			<p>
				<i style="padding-right: 4px;" class="fa fa-comments-o"></i><a style="display: inline-block;" href="#disqus_thread"> Send inn en kommentar eller rettelse om denne siden</a>
			</p>
			
			<!-- WIKIPEDIA -->
			<p><i class="fa fa-code"></i> <a data-toggle="collapse" data-target="#wikipedia-reference">
			  Siteringskode for Wikipedia <span class="caret"></span>
			</a></p>
			<p id="wikipedia-reference" class="collapse">
                <textarea class="form-control col-md-12" rows="8">&#123;&#123;cite web |url={{lodspk.local.curie}} |title={{first.main.label.value}} |author=Avdeling for spesialsamlinger |accessdate={{lodspk.currentDate}} |publisher=University of Bergen Library&#125;&#125;</textarea>
			</p>
			
			<!-- QR CODE -->
			<p><i class="fa fa-qrcode"></i>
				<a data-toggle="collapse" data-target="#qrcode">QR-kode <span class="caret"></span></a>
			</p>
			<div id="qrcode" class="collapse"></div>
			<script type="text/javascript">
			$('#qrcode').qrcode({
				render: 'image',
				minVersion: 3,
				maxVersion: 40,
				size: 150,
				quiet: 2,
				radius: 0.3,
				ecLevel: 'Q',
				fill: '#00a7df',
				text: '{{lodspk.local.curie}}'
			});
			</script>
			
			<!-- EMAIL FEEDBACK -->
			<p><i class="fa fa-exclamation-triangle"></i> <a href="mailto:mslib@uib.no?subject=Marcus - tilbakemelding på {{first.main.identifier.value}}&body=Tilbakemeldingen gjelder: {{first.main.identifier.value}} ({{lodspk.local.curie}}). La lenken være med, vi trenger den for å svare.">Feil? Gi oss beskjed!</a></p>
			
			<!-- DATA VIEW -->
			<button type="button" class="btn btn-xs btn-default" data-toggle="modal" data-target="#full-meta-view">Vis data</button>
