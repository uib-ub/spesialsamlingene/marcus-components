<script src="{{lodspk.home}}js/sweetalert/lib/sweet-alert.min.js"></script> 
<link rel="stylesheet" type="text/css" href="{{lodspk.home}}js/sweetalert/lib/sweet-alert.css">
<script type="text/javascript" src="{{lodspk.home}}js/node_modules/parsleyjs/src/i18n/no.js"></script>
<script type="text/javascript" src="{{lodspk.home}}js/node_modules/parsleyjs/dist/parsley.min.js"></script>

			<div class="simpleCart_shelfItem row">
				<div class="col-md-12">
				<h5>Bestill</h5>
				<div class="row">
				<form role="form">

				    <input class="item_name" hidden value="{{ first.main.title.value }}">
				    <input class="item_link" hidden value="{{lodspk.local.curie}}">
				    <input class="item_thumb" hidden value="{{first.thumb.thumb.value}}">
				    <input class="item_sign" hidden value="{{ first.main.identifier.value }}">
					<div class="col-md-3">
					<input type="number" value="1" min="1" max="50" style="width: 100%;" class="form-control item_Quantity" required="true"> 
				    </div>
				    <div class="col-md-9">
				    <select class="form-control item_type" required="required">
                        <option value="error">Velg produkt</option>
					    <option value="13x18">Trykk 13x18 - 100kr</option>
					    <option value="18x24">Trykk 18x24 - 150kr</option>
					    <option value="24x30">Trykk 24x30 - 200kr</option>
					    <option value="30x40">Trykk 30x40 - 250kr</option>
					    <option value="40x50">Trykk 40x50 - 350kr</option>
					    <option value="50x60">Trykk 50x60 - 450kr</option>
					    <option value="60x70">Trykk 60x70 - 550kr</option>
					    <option value="70x100">Trykk 70x100 - 650kr</option>
                                            <option value="fil">Høyoppløselig fil - 500kr</option>
				    </select>
				    </div>
          </div>
          <div class="row">
				    <div class="col-md-6">
  				    <span style="margin-top: 8px;" class="item_price"></span>
  				  </div>
				    <div style="text-align: right;" class="col-md-6">
					    <a style="margin-top: 10px;" class="btn btn-default item_add" href="javascript:;"> Legg i handlekurv </a><br>
					    <a class="btn btn-link" href="/cart.html">Gå til handlekurven <span style="margin-top: 8px;" class="label label-primary simpleCart_quantity"></span></a>
					  </div>
            <div class="clearfix"></div>
					</div>
          <!--<div class="alert alert-warning"><p><i class="fa fa-warning"></i> Siste frist for bestilling av bilder fra Billedsamlingen før sommerferien er 23. juni.</p></div>-->
				</form>

				</div>
			</div>
			
