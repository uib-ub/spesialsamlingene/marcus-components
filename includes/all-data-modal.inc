	<div id="full-meta-view" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			        <h4 class="modal-title" id="myModalLabel">{% for row in models.main %}{{ row.title.value }}{% endfor %} - data</h4>
			      </div>
				  <div class="modal-body">
				  <p>Dette er våre data om <i>{% for row in models.main %}{{ row.title.value }}{%endfor%}</i>. Her får du også et innblikk i hvordan vår samling er modellert og relasjonene er bygget opp.
					<table style="table-layout:fixed; word-wrap: break-word;" class="table table-striped" about="{{uri}}">
						<thead>
							<tr><th>Subjekt</th><th>Predikat</th><th>Objekt</th></tr>
						</thead>
				    {% for row in models.po %}
							<tr>
								<td>{%if forloop.first%}<a href='{{lodspk.this.value}}'>{{lodspk.this.curie}}</a>{%endif%}</td>
								<td><a href='{{row.p.value}}'>{{row.p.curie}}</a></td>
								<td>
									{%if row.o.uri == 1 %}
									<a rel='{{row.p.value}}' href='{{row.o.value}}'>{{row.o.curie}}</a>
									{%else%}
									<span property='{{row.p.value}}'>{{row.o.value}}</span>
									{%endif%}
								</td>
							</tr>
					{% endfor %}
	
					{% for row in models.sp %}
							<tr>
								<td><a href='{{row.s.value}}'>{{row.s.curie}}</a></td>
								<td><a rev='{{row.s.value}}' href='{{row.p.value}}'>{{row.p.curie}}</a></td>
								<td>{%if forloop.first%}<a href='{{lodspk.this.value}}'>{{lodspk.this.curie}}</a>{%endif%}</td>
							</tr>
					{% endfor %}
						<tfoot>
							<tr><th>Subjekt</th><th>Predikat</th><th>Objekt</th></tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
