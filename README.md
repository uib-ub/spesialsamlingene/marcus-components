# Marcus components

This repository contains all queries, templates and extra views for the University of Bergens special collection page [Marcus](https://marcus.uib.no). The  frontend framework used is [Bootstrap v3](https://getbootstrap.com/docs/3.3/).

This repo runs CI and builds the latest commit to [marcus test server](https://marcus.test.uib.no/home) on commit where changes can be viewed. It also contains (*manual*, on *master*) deployment to [marcus.uib.no](https://marcus.uib.no).


A possible workflow is: 
* [open an issue](https://git.app.uib.no/uib-ub/spesialsamlingene/marcus-components/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
* create merge request (from issue in [web-GUI](https://git.app.uib.no/uib-ub/spesialsamlingene/marcus-components/-/issues))
* push to merge request
* review/confirm changes/feature in [the test server](https://marcus.test.uib.no/home)
* merge request to master
* Go to the mirror at https://itgit.app.uib.no/ub/spesialsamlingene/marcus-components and click `marcus_prod` which deploys to https://marcus.uib.no

![Image of deployment](docs/deploy_prod.png)

## Related projects

* [ansible-marcus](https://git.app.uib.no/uib-ub/spesialsamlingene/ansible-marcus) sets up the marcus infrastructure, including admin endpoint.
* [marcus search client (**marcus** branch)](https://git.app.uib.no/uib-ub/spesialsamlingene/marcus-search-client/-/tree/marcus) is the elasticsearch client.
* [lodspeakr](https://git.app.uib.no/uib-ub/spesialsamlingene/marcus-lodspeakr) is the framework which uses the components. The components repo is usually placed in the components in the main directory.
* [admin-components](https://git.app.uib.no/uib-ub/spesialsamlingene/admin-components), [katalog.skeivtarkiv.no](https://git.app.uib.no/uib-ub/skeivtarkiv/katalog.skeivtarkiv.no) are other components repos
