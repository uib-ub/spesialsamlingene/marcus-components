{%include "../../includes/header.inc"%}

<div class="container main-body">
	<div itemscope itemtype="http://schema.org/Person" class="row">
		<div class="col-md-3">
			<div class="bs-sidebar hidden-xs hidden-sm affix">
				
				<div class="col-md-10 col-md-offset-1">
					{% if first.main.img.value %}
					<img itemprop="http://schema.org/image" class="img-circle img-responsive" src="{{first.main.img.value}}" />
					{% endif %}
					<br>
					<p class="text-center"><a itemprop="url" href="{{lodspk.local.curie}}"><strong itemprop="name">{{ first.main.name.value }}</strong></a></p>
				</div>
				<br>
		
				
				{% if first.main.altname %}
					<p class="text-center"><strong class="text-muted">Også kjent som</strong><br>
  					<ul class="list-unstyled text-center">
  						{% for row in models.main %}
    						{% ifchanged row.altname.value %}
    						  <li><i>{{row.altname.value}}</i></li>
    						{% endifchanged %}
  						{% endfor %}
						</ul>
					{% endif %}
		
				<div class="col-md-12">					
					{% if not first.main.birthdate.value | null || not first.main.birthyear.value | null  %}
					<p class="text-center"><strong class="text-muted">Født</strong><br>
						{% if first.main.birthdate.value != 0 %}<span itemprop="http://schema.org/birthDate">{{first.main.birthdate.value}}
						</span>{% endif %}
						{% if first.main.birthyear.value != 0 %}<span itemprop="http://schema.org/birthYear">{{first.main.birthyear.value}}
						</span>{% endif %}
					</p>
					{% endif %}
					
					{% if first.main.deathdate.value || first.main.deathyear.value %}
					<p class="text-center"><strong class="text-muted">Død</strong><br>
						{% if first.main.deathdate.value != 0 %}<span itemprop="http://schema.org/deathDate">{{first.main.deathdate.value}}</span>{% endif %}
  						{% if first.main.deathyear.value != 0 %}<span itemprop="http://schema.org/deathYear">{{first.main.deathyear.value}}
						</span>{% endif %}
					</p>
					{% endif %}
					
					{% if first.main.birthplaceuri %}
					<p class="text-center"><strong class="text-muted">Fødested</strong><br>
						<a href="{{first.main.birthplaceuri.value}}">{{first.main.birthplace.value}}</a>
					</p>
					{% endif %}
					
					{% if first.main.deathplaceuri %}
					<p class="text-center"><strong class="text-muted">Dødssted</strong><br>
						<a href="{{first.main.deathplaceuri.value}}">{{first.main.deathplace.value}}</a>
					</p>
					{% endif %}
										
					{% if first.main.profession %}
					<p class="text-center"><strong class="text-muted">Yrke</strong><br>
  					<ul class="list-unstyled text-center">
  						{% for row in models.main %}
  						  {% ifchanged row.profession.value %}
    							<li>{{row.profession.value}}</li>
    						{% endifchanged %}
  						{% endfor %}
						</ul>
					{% endif %}
					
					
					{% if first.main.spouseUri %}
					<p class="text-center"><strong class="text-muted">Ektefelle</strong><br>
						<a href="{{first.main.spouseUri.value}}">{{first.main.spouseName.value}}</a>
					</p>
					{% endif %}
					
					{% if models.parents|length != 0 %}
					<p style="margin-bottom: 0;" class="text-center"><strong class="text-muted">Foreldre</strong></p>
						<ul class="list-unstyled text-center">
						{% for row in models.parents %}
							<li><a href="{{row.uri.value}}">{{row.name.value}}</a></li>
						{% endfor %}
						</ul>
					{% endif %}
					
					{% if models.children|length != 0 %}
					<p style="margin-bottom: 0;" class="text-center"><strong class="text-muted">Barn</strong></p>
						<ul class="list-unstyled text-center">
						{% for row in models.children %}
							<li><a href="{{row.uri.value}}">{{row.name.value}}</a></li>
						{% endfor %}
						</ul>
					{% endif %}
					
					{% if models.webresources|length != 0 %}
					<p class="text-center"><strong class="text-muted">Nettsider</strong><br>
						{% for row in models.webresources %}
						<a target="_blank" href="{{row.uri.value}}">{{row.label.value}}</a> <i class="text-muted fa fa-external-link"></i><br>
						{% endfor %}
					</p>
					{% endif %}	
					
					{% if models.inscheme|length != 0 %}
					<p class="text-center"><strong class="text-muted">Del av:</strong><br>
					{% for row in models.inscheme %}
						<a href="{{row.uri.value}}">{{row.label.value}}</a>
					{% endfor %}
					</p>
					<hr>
					{% endif %}	
				</div>
			
				<div class="clearfix"></div>
				
				<!-- WIKIPEDIA CITATION -->
				<p class="text-center"><i class="fa fa-code"></i> <a data-toggle="collapse" data-target="#wikipedia-reference">Siteringskode for Wikipedia</a></p>

				<p id="wikipedia-reference" class="collapse">
                <textarea class="form-control col-md-12" rows="8">&#123;&#123;cite web |url={{lodspk.local.curie}} |title={{first.main.label.value}} |author=Avdeling for spesialsamlinger |accessdate={{lodspk.currentDate}} |publisher=University of Bergen Library&#125;&#125;</textarea>
				</p>
				
				<!-- INTERNAL CITATION -->
<!--
				<p class="text-center"><i class="fa fa-code"></i> <a data-toggle="collapse" data-target="#spreadsheet-reference">Intern sitering</a></p>
				
				<p id="spreadsheet-reference" class="collapse">
                <textarea class="form-control col-md-12" rows="5">{{lodspk.this.value}} ({{ first.main.name.value }})</textarea>
				</p>
-->
			
				<div class="text-center">
				<!-- EMAIL FEEDBACK -->
				<button type="button" class="btn btn-xs btn-link"><i class="fa fa-exclamation-triangle m-button-black"></i> <a href="mailto:mslib@uib.no?subject=Marcus - tilbakemelding på {{first.main.identifier.value}}&body=Tilbakemeldingen gjelder: {{first.main.identifier.value}} ({{lodspk.local.curie}}). La lenken være med, vi trenger den for å svare.">Feil? Gi oss beskjed!</a></button>
				<!-- ALL DATA -->
				<button type="button" class="btn btn-xs btn-link all-data" data-toggle="modal" data-target="#full-meta-view"><i class="fa fa-heart m-button-red pulse animated"></i> Vis data</button>
				</div>
				<hr>
			
				<!-- <div class="browse-sidebar">				
					<span class="h4">Bla i <span class="marcus-logo">Marcus</span>
					</span>
					<ul class="list-unstyled">
						{% include "../../includes/nav.inc" %}
					</ul>
				</div>	-->
			</div> 
				<!-- End Sidebar -->
		</div>
			
		<div class="col-md-9">
	    	<h1>{{ first.main.name.value }}</h1>
	    	
			
			
	    	{% if first.main.description.value %}<div class="decode-description" itemprop="http://schema.org/description">{% autoescape off %}{{first.main.description.value|decodedescription}}{% endautoescape %}</div>{% endif %}
    	
				
			{% if not first.dbpedia.sameas.dbparticle | null %}
			<div class="col-md-12 well dbpedia">
		    {% for i in models.dbpedia.sameas %}
		    	{% ifchanged i.dbparticle.value %}
		    	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 pull-right"><img class="img-rounded img-responsive" src="{{i.thumbnail.value}}"></div>
		    	<p class="text-muted">{% autoescape off %}{{i.abstract.value}}{% endautoescape %}<p>
		    	<p><i class="fa fa-quote-left"></i> Fra Wikipedia (<a href="{{i.dbparticle.value}}"><small>{{i.dbparticle.value}}</small></a>)</p>
				{% endifchanged %}
		    {% endfor %}
				<div class="clearfix"></div>
			</div>
			{% endif %}
			
			{% if models.ms|length != 0 %}
			<hr>
			<p><strong>{{ first.main.name.value }} er relatert til disse samlingene.</strong> Disse samlingene er ikke tilgjengelig i Marcus, men du kan finne mer informasjon i den gamle <a href="http://www.uib.no/filearchive/manuskriptkatalogen-ms-1-2097.pdf" target="_blank">Manuskriptkatalogen (pdf)</a>. 
			<br>
				<ul class="list-inline">
				{% for row in models.ms %}
				{% if row.label.literal == 1 %}
					<li><i class="fa fa-dot-circle-o"></i> {{row.label.value}}</li>
				{% endif %}
			    {% endfor %}
				</ul>
			</p>
			<hr>
			{% endif %}
			
			{% if models.collections|length != 0 %}
			<div class="collections">
			<h3>Relaterte samlinger</h3>
			{% for row in models.collections %}
		
			   {% ifchanged row.uri.value %}
			   <div style="padding-bottom: 15px; margin-bottom: 15px; border-bottom: 1px solid #eee;" class="col-md-12">
		
		
				    	<h4><i class="fa fa-archive"></i><a href="{{ row.uri.value }}"> {% if row.label && row.label.value != "" %}{{row.label.value}}{% else %} {{row.identifier.value}}{%endif%}</a></h4>
				    	{% if row.description.value %}<p>{{row.description.value|striptags|truncatewords:35}}</p>{% endif %}
						
						<p><span class="label label-info">Samling</span> <span class="text-muted"><i style="margin-right: 5px;" data-trigger="hover" data-toggle="tooltip" data-placement="left" title="Arkivnøkkel" class="fa fa-key"></i>{{row.identifier.value}}</span></p>
					</div>
				{% endifchanged %}
				
			{% endfor %}
			</div>
			{% endif%}
	
		<!-- DEPICTIONS -->
		{% if models.depictions|length != 0 %}
		<div class="col-md-12">
			<div class="row th-grid">	
			<h3>Avbildet på:</h3>
	
			{% for row in models.depictions %}
		    	<div class="col-md-4 col-lg-4">
		        	<div class="thumbnail">      
			        	<a href="{{ row.uri.value }}">
			        	<div class="caption">
			      			<h3>{{ row.label.value }}</h3>
			      		</div></a>
						<a href="{{ row.uri.value }}"><img class="img-responsive img-rounded" {% if row.img %}src="{{row.img.value}}"{%else%}data-src="holder.js/250x250"{%endif%}  alt=""></a>
			  		</div>
		  		</div>
		  	{% endfor %}
			</div>
		<script type="text/javascript">
		    $("[rel='tooltip']").tooltip();    
		 
		    $('.thumbnail').hover(
		        function(){
		            $(this).find('.caption').fadeIn(250);
		        },
		        function(){
		            $(this).find('.caption').fadeOut(250);
		        }
		    );      
		</script>
		</div>
		{% endif %}
	
	<!-- RELATED RESOURCES -->
	{% if models.related|length != 0 %}
	<div class="related">
	<h3>Knyttet til {{first.count.total.value}} objekt:</h3>
	    {% if first.count.total.value > 100 %}
	    <div>
	        <script>
		    if ("{{lodspk.args.arg1}}".length < 1)
		    	{page = 0;}
		    	else {page = "{{lodspk.args.arg1}}";}
				if ("{{lodspk.args.arg1}}".length > 0)
				{page=parseInt(page)}
				else {}
		    var offset = 100;
		  	
		    var nextPage = page+offset;
			var prevPage = page-offset;
var next_uri = '{{lodspk.home}}instance/person/'+'{{lodspk.args.arg0}}'+'/' + nextPage;
var prev_uri = '{{lodspk.home}}instance/person/'+'{{lodspk.args.arg0}}'+'/' + prevPage;
			document.write('<ul class="pager">');
				if (prevPage === 0){
					document.write('<li><a href="{{lodspk.home}}instance/person/'+'{{lodspk.args.arg0}}"" id="prev">Forrige 100</a></li>');
				}		
				else if ("{{lodspk.args.arg1}}".length > 1){
					document.write('<li><a href="" class="prev">Forrige 100</a></li>');
					$(".prev").attr("href",prev_uri);
				};
		
				if ({{first.count.total.value}} > nextPage){
					document.write('<li><a href="" class="next">Neste 100</a></li>');
					$(".next").attr("href",next_uri);
				};
			document.write('</ul>');
			</script>	
		 </div>		
    {% endif %}
    
    {% for row in models.related %}
	<div itemscope itemtype="{% if row.classLabel.value == "Fotografi" %}http://schema.org/Photograph{%else%}{% if row.classLabel.value != "Samling" %}http://schema.org/CreativeWork{%else%}http://schema.org/CollectionPage{%endif%}{%endif%}" class="row">
	   {% ifchanged row.uri.value %}
	   <div style="padding-bottom: 15px; margin-bottom: 15px; border-bottom: 1px solid #ddd;" class="col-md-12">
	    	{% if row.classLabel.value != "Samling" %}
		    	<div class="col-md-3">
		    		<a href="{{row.uri.value}}"><img class="pull-left img-responsive img-rounded" {% if row.img %}{% else %}data-src="holder.js/100%x150/text:Ingen forhåndsvisning"{% endif %} src="{{row.img.value}}" /></a>
		    	</div>
	    	{% endif %}
	    	{% if row.makerNames %}
		    		<p><i style="margin-right: 5px;" data-trigger="hover" data-toggle="tooltip" data-placement="left" title="Personer knyttet til samlingen eller dokumentet" class="fa fa-users"></i> <strong>{{row.makerNames.value}}</strong></p>
		    	{% endif %}
		    	<h4>{% if row.classLabel.value == "Samling" %}<i class="fa fa-level-down"></i>{% endif %}<a itemprop="url" href="{{ row.uri.value }}">  <span itemprop="name">{% if row.title && row.title.value != "" %}{{row.title.value}}{% else %}{% if row.label && row.label.value != "" %}{{row.label.value}}{% else %} {{row.identifier.value}}</span>{%endif%}{%endif%}</a></h4>
		    	{% if row.description.value %}<p itemprop="description">{{row.description.value|striptags|truncatewords:35}}</p>{% endif %}
		    	
		    	{% if row.subjectLabels %}
		    		<p class="text-muted"><i class="fa fa-tags"></i> <i itemprop="keywords">{{row.subjectLabels.value}}</i></p>
				{% endif %}
				
				<p> {% if row.classLabel %}<span class="label label-info">{{row.classLabel.value}}</span> {% endif %} {% if row.hasTranscription.value == "true" %}<span data-trigger="hover" data-toggle="tooltip" data-placement="left" title="Dokumentet har en moderne avskrift" class="label label-primary">Transkribert</span>{% endif %} {% if row.hasPage.value == "true" %}<span data-trigger="hover" data-toggle="tooltip" data-placement="left" title="Dokumentet er digitalisert" class="label label-default">Faksimile</span>{% endif %} <span class="text-muted"><i style="margin-right: 5px;" data-trigger="hover" data-toggle="tooltip" data-placement="left" title="Arkivnøkkel" class="fa fa-key"></i>{{row.identifier.value}}</span></p>
			</div>
		{% endifchanged %}
		</div>
	{% endfor %}
	

    </div>
    {% endif %}

 <script>$('[data-toggle="tooltip"]').tooltip({'placement': 'top'});</script>
      
    {% if first.count.total.value > 100 %}
	    <div>
	        <script>
		    if ("{{lodspk.args.arg1}}".length < 1)
		    	{page = 0;}
		    	else {page = "{{lodspk.args.arg1}}";}
				if ("{{lodspk.args.arg1}}".length > 0)
				{page=parseInt(page)}
				else {}
		    var offset = 100;
		  	
		    var nextPage = page+offset;
			var prevPage = page-offset;
var next_uri = '{{lodspk.home}}instance/person/'+'{{lodspk.args.arg0}}'+'/' + nextPage;
var prev_uri = '{{lodspk.home}}instance/person/'+'{{lodspk.args.arg0}}'+'/' + prevPage;
			document.write('<ul class="pager">');
				if (prevPage === 0){
					document.write('<li><a href="{{lodspk.home}}instance/person/'+'{{lodspk.args.arg0}}"" id="prev">Forrige 100</a></li>');
				}		
				else if ("{{lodspk.args.arg1}}".length > 1){
					document.write('<li><a href="" class="prev">Forrige 100</a></li>');
					$(".prev").attr("href",prev_uri);
				};
		
				if ({{first.count.total.value}} > nextPage){
					document.write('<li><a href="" class="next">Neste 100</a></li>');
					$(".next").attr("href",next_uri);
				};
			document.write('</ul>');
			</script>	
		 </div>		
    {% endif %}
      
<div id="full-meta-view" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="myModalLabel">{{ first.main.name.value }} - data</h4>
		      </div>
			  <div class="modal-body">
			  <p>Dette er våre data om <i>{{ first.main.name.value }}</i>. Her får du også et innblikk i hvordan vår samling er modellert og relasjonene er bygget opp.
				<table class="table table-striped" about="{{uri}}">
					<thead>
						<tr><th>Subjekt</th><th>Predikat</th><th>Objekt</th></tr>
					</thead>
			    {% for row in models.po %}
						<tr>
							<td>{%if forloop.first%}<a href='{{lodspk.this.value}}'>{{lodspk.this.curie}}</a>{%endif%}</td>
							<td><a href='{{row.p.value}}'>{{row.p.curie}}</a></td>
							<td>
								{%if row.o.uri == 1%}
								<a rel='{{row.p.value}}' href='{{row.o.value}}'>{{row.o.curie}}</a>
								{%else%}
								<span property='{{row.p.value}}'>{{row.o.value}}</span>
								{%endif%}
							</td>
						</tr>
				{% endfor %}

				{% for row in models.sp %}
						<tr>
							<td><a href='{{row.s.value}}'>{{row.s.curie}}</a></td>
							<td><a rev='{{row.s.value}}' href='{{row.p.value}}'>{{row.p.curie}}</a></td>
							<td>{%if forloop.first%}<a href='{{lodspk.this.value}}'>{{lodspk.this.curie}}</a>{%endif%}</td>
						</tr>
				{% endfor %}
					<tfoot>
						<tr><th>Subjekt</th><th>Predikat</th><th>Objekt</th></tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
      
      </div></div></div>
      {%include "../../includes/footer.inc"%}
  </body>
</html>
