{%include "../../includes/header.inc"%}
    <div itemscope itemtype="http://schema.org/CreativeWork" class="container main-body">
    	
    <div class="row">
	<div class="col-md-3">
			<div class="bs-sidebar hidden-xs hidden-sm affix">

			   	{% if not models.collections | null %}
	 			<p class="text-center"><strong class="text-muted">Del av samling:</strong><br>
	 				{% for row in models.collections %}
					<a target="_blank" href="{{row.uri.value}}">{{row.label.value}}</a><br>
					{% endfor %}
				</p>
				{% endif %}
				<hr>
				<p><i class="fa fa-exclamation-triangle"></i> <a href="mailto:billed@uib.no?subject=Marcus - tilbakemelding på {{first.main.identifier.value}}&body=Tilbakemeldingen gjelder: {{first.main.identifier.value}} ({{lodspk.local.curie}}). La lenken være med, vi trenger den for å svare.">Feil? Gi oss beskjed!</a></p>

				<button type="button" class="btn btn-xs btn-default" data-toggle="modal" data-target="#full-meta-view">Vis data</button>
				
				<hr>
				
				<div class="browse-sidebar">
					<h4>Bla i <span class="marcus-logo">Marcus</span></h4>
					<ul class="list-unstyled">
						{% include "../../includes/nav.inc" %}
					</ul>
				</div>			
				
				</div> <!-- End Sidebar -->
			</div>
			
			<div class="col-md-9">

	   <div style="margin-top: 50px;" class="catalogue-card"> 
	    <h1>{{ first.main.label.value }}</h1>
    
    
    {% if not first.maker.maker.value | null %}
    <p><i class="fa fa-camera"></i> 
    {% for row in models.maker %}
    {%if !forloop.last %} - {%endif%}
    <a href="{{ row.maker.value }}">{{ row.makerName.value }}</a>
    {% endfor %}
    </p>
    {% endif %}
    
    {% for row in models.date %}
    <p>
    	{% if row.created %}
    		<span>Dato: {{ row.created.value }}</span>
    	{% endif %}
    	{% if row.available %}
    		<span>Tilgjengelig fra: {{ row.available.value }}</span>
    	{% endif %}
    	{% if row.madeAfter %}
    		<span>Laget etter: {{ row.madeAfter.value }}</span>
    	{% endif %}
    	{% if row.madeBefore %}
    		<span>Laget før: {{ row.madeBefore.value }}</span>
    	{% endif %}
    </p>
    
    {% endfor %}
    
    <p>Signatur: <a href="{{lodspk.this.value}}">{{ first.main.identifier.value }}</a></p>

	{% if not models.rodenr| null %}
	<p>Rodenummer:
	<ul class="subjects-inline-list">
	{% for row in models.rodenr %}
    	<li><i class="fa fa-map-marker"></i> {{row.label.value}}</li>
	{% endfor %} 
	</ul>
	</p>
	{%endif%}
		
    {% for row in models.main %}
    {% autoescape off %}    <div class="decode-description">{{ row.description.value|decodedescription }}</div>    {% endautoescape %}
    {% endfor %}
    

        {% if models.relatedplaces|length != 0 %}
		<p>Relatert sted:
			<ul class="list-inline">
				{% for row in models.relatedplaces %}
				{%ifchanged row.uri.value%}
		    	<li><i class="fa fa-map-marker"></i> <a href="{{ row.uri.value }}">{{ row.label.value }}</a></li>
		    	{%endifchanged%}
				{% endfor %} 
			</ul>
		</p>
		{% endif %}        

        {% if models.relations|length != 0 %}
		<p>Relatert til:
			<ul class="list-inline">
				{% for row in models.relations %}
				{%ifchanged row.uri.value%}
		    	<li class="label label-marcus-subject"><a href="{{ row.uri.value }}">{{ row.label.value }}</a></li>
		    	{%endifchanged%}
				{% endfor %} 
			</ul>
		</p>
		{% endif %}
        
		{% if not models.references| null %}
		<p>Referanse(r):
		<ul class="subjects-inline-list">
		{% for row in models.references %}
        	<li><i class="fa fa-folder"></i> {{row.label.value}}</li>
		{% endfor %} 
		</ul>
		</p>
		{%endif%}

		{% if models.subjects|length != 0 %}
		<p>Emne(r):
			<ul class="list-inline">
				{% for row in models.subjects %}
				{%ifchanged row.uri.value%}
		    	<li class="label label-marcus-subject"><a href="{{ row.uri.value }}">{{ row.label.value }}</a></li>
		    	{%endifchanged%}
				{% endfor %} 
			</ul>
		</p>
		{% endif %}
		
		<!-- RELATED PERSONS -->
		{% if models.relatedpersons|length != 0 %}
			<div class="row">	
				<div id="depicted" class="col-md-12">
				<h4>Relatert person:</h4>
				{% for row in models.relatedpersons %}
				{% ifchanged row.uri.value %}
				<div class="col-md-3 depicted">
				{% if row.img %}<div class="col-md-10 col-md-offset-1"><img class="img-responsive img-border img-circle" src="{{row.img.value}}" /></div>{% endif %}
		    		<p><strong><a href="{{ row.uri.value }}" >{{row.label.value}}</a></strong><br>
		    		{% if row.birthDate.value || row.deathDate.value %}
	    	{% if row.birthDate %}<small>({{ row.birthDate.value }}{% else %}<small>(&nbsp;&nbsp;&nbsp;&nbsp;-{% endif %}
	    	{% if row.deathDate %}- {{ row.deathDate.value }})</small>{% else %}-&nbsp;&nbsp;&nbsp;&nbsp;)</small>{% endif %}
	    	{% else %}</small>{% endif %}</p>	
		    		{% if row.description %}<p>{{row.description.value|truncatewords:15}}</p>{% endif %}
	    		</div>
	    		{% endifchanged %}
				{% endfor %}
				</div>	
			</div>
		{% endif %}
				
		{% if models.relatedresources|length != 0 %}
		<h4 style="border-bottom: 1px solid red;">Fotografi fra samme område</h4>
		<div class="row">
	    {% for row in models.relatedresources %}
	    	<div class="col-md-6 col-lg-4">
	        	<div>
					<a class="thumbnail" href="{{ row.uri.value }}"><img class="img-rounded" {% if row.image %}src="{{row.image.value}}"{%else%}data-src="holder.js/250x250"{%endif%} alt=""></a>
	      		<div class="caption">
	      		<h3>{{ row.label.value|truncatechars:20 }}</h3>
	      		</div>
		  		</div>
	      	</div>
	    {% endfor %}
	    </div>
	    {% endif %}
    
		{% if not models.internalnotes | null %}
		<p>Interne noter(r):
		<ul class="subjects-inline-list">
		{% for row in models.internalnotes %}
        	<li><i class="fa fa-bullseye"></i> {{row.label.value}}</li>
		{% endfor %} 
		</ul>
		</p>
		{%endif%}
</div>
		
		
	<!-- PLACES/MAP -->
	{% if models.places|length != 0 %}
	<div class="row">
		<div id="places" class="col-md-12 container">
			{% include "../../includes/place.inc" %}
		</div>
	</div>
	{% endif %}

	<!-- disable DISQUS -->
        {% include "../../includes/disqus.inc" %}
	
<div id="full-meta-view" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="myModalLabel">{% for row in models.main %}{{ row.label.value }}{% endfor %} - data</h4>
		      </div>
			  <div class="modal-body">
			  <p>Dette er våre data om {% for row in models.main %}{{ row.label.value }}{%endfor%}. Her får du også et innblikk i hvordan vår samling er modellert og relasjonene er bygget opp.
				<table style="table-layout:fixed; word-wrap: break-word;" class="table table-striped" about="{{uri}}">
					<thead>
						<tr><th>Subjekt</th><th>Predikat</th><th>Objekt</th></tr>
					</thead>
			    {% for row in models.po %}
						<tr>
							<td>{%if forloop.first%}<a href='{{lodspk.this.value}}'>{{lodspk.this.curie}}</a>{%endif%}</td>
							<td><a href='{{row.p.value}}'>{{row.p.curie}}</a></td>
							<td>
								{%if row.o.uri == 1%}
								<a rel='{{row.p.value}}' href='{{row.o.value}}'>{{row.o.curie}}</a>
								{%else%}
								<span property='{{row.p.value}}'>{{row.o.value}}</span>
								{%endif%}
							</td>
						</tr>
				{% endfor %}

				{% for row in models.sp %}
						<tr>
							<td><a href='{{row.s.value}}'>{{row.s.curie}}</a></td>
							<td><a rev='{{row.s.value}}' href='{{row.p.value}}'>{{row.p.curie}}</a></td>
							<td>{%if forloop.first%}<a href='{{lodspk.this.value}}'>{{lodspk.this.curie}}</a>{%endif%}</td>
						</tr>
				{% endfor %}
					<tfoot>
						<tr><th>Subjekt</th><th>Predikat</th><th>Objekt</th></tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>    

	</div>
	</div>
    </div>
    </div>
    	{%include "../../includes/footer.inc"%}
    
  </body>
</html>
