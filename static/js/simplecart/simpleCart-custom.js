/*~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~

	CUSTOM simpleCart
	
~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~*/
		    simpleCart({
		    checkout: {
		    type: "SendForm",
		    method: "GET" ,
			url: "http://marcus.uib.no/cart.html"
		    },
		cartColumns: [
		        { view: 'image' , attr:'thumb', label: false} ,
		        { attr: "name" , label: "Name" } ,
		        { attr: "type" , label: "Order type" } ,
		        { view: 'link' , label: 'Details' , attr: 'link' , text: 'details' } ,
		        { attr: "price" , label: "Price", view: 'currency' } ,
		        { view: "decrement" , label: false , text: "-" } ,
		        { attr: "quantity" , label: "Qty" } ,
		        { view: "increment" , label: false , text: "+" } ,
		        { attr: "total" , label: "SubTotal", view: 'currency' } ,
		        { view: "remove" , text: "Remove" , label: false }
		    ],
				cartStyle: 'table',
				currency: "NOK",    
		});

		 
		$('.item_type').on('change', function() {
		  if ($('.item_type').val() === '13x18') {
		    var pricewithtax = 100*1.25 ;
		    $('.item_price').replaceWith("<span class=\"item_price\"><strong>" + pricewithtax + " kr pr. stk</strong><br>inkl. mva.</span>");
		  }
		  if ($('.item_type').val() === '18x24') {
		    var pricewithtax = 150*1.25 ;
		    $('.item_price').replaceWith("<span class=\"item_price\"><strong>" + pricewithtax + " kr </strong><br>inkl. mva.</span>");
		  }
		  if ($('.item_type').val() === '24x30') {
		    var pricewithtax = 200*1.25 ;
		    $('.item_price').replaceWith("<span class=\"item_price\"><strong>" + pricewithtax + " kr pr. stk</strong><br>inkl. mva.</span>");
		  }
		  if ($('.item_type').val() === '30x40') {
		    var pricewithtax = 250*1.25 ;
		    $('.item_price').replaceWith("<span class=\"item_price\"><strong>" + pricewithtax + " kr pr. stk</strong><br>inkl. mva.</span>");
		  }
		  if ($('.item_type').val() === '40x50') {
		    var pricewithtax = 350*1.25 ;
		    $('.item_price').replaceWith("<span class=\"item_price\"><strong>" + pricewithtax + " kr pr. stk</strong><br>inkl. mva.</span>");
		  }
		  if ($('.item_type').val() === '50x60') {
		    var pricewithtax = 450*1.25 ;
		    $('.item_price').replaceWith("<span class=\"item_price\"><strong>" + pricewithtax + " kr pr. stk</strong><br>inkl. mva.</span>");
		  }
		  if ($('.item_type').val() === '60x70') {
		    var pricewithtax = 550*1.25 ;
		    $('.item_price').replaceWith("<span class=\"item_price\"><strong>" + pricewithtax + " kr pr. stk</strong><br>inkl. mva.</span>");
		  }
		  if ($('.item_type').val() === '70x100') {
		    var pricewithtax = 650*1.25 ;
		    $('.item_price').replaceWith("<span class=\"item_price\"><strong>" + pricewithtax + " kr pr. stk</strong><br>inkl. mva.</span>");
		  }
		  if ($('.item_type').val() === 'fil') {
		    var pricewithtax = 500*1.25 ;
		    $('.item_price').replaceWith("<span class=\"item_price\"><strong>" + pricewithtax + " kr pr. stk</strong><br>inkl. mva.</span>");
		  }
		});
		
		simpleCart.bind( 'beforeAdd' , function( item ){
		  if( item.get( 'type' ) === '13x18' ){
		    item.price( 100 );
		  } 
		  if( item.get( 'type' ) === '18x24' ){
		    item.price( 150 );
		  } 
		  if( item.get( 'type' ) === '24x30' ){
		    item.price( 200 );
		  }
		  if( item.get( 'type' ) === '30x40' ){
		    item.price( 250 );
		  }
		  if( item.get( 'type' ) === '40x50' ){
		    item.price( 350 );
		  }
		  if( item.get( 'type' ) === '50x60' ){
		    item.price( 450 );
		  }
		  if( item.get( 'type' ) === '60x70' ){
		    item.price( 550 );
		  }
		  if( item.get( 'type' ) === '70x100' ){
		    item.price( 650 );
		  }
		  if( item.get( 'type' ) === 'fil' ){
		    item.price( 500 );
		  }
		  if( item.get( 'type' ) === 'error' ) {
		    swal('Du må velge produkt først','' ,'warning'); //Uses sweetalert.js to add some flare to the alert modal
		    return false;
		  }
		});
			
		simpleCart.bind( 'afterAdd' , function( item ){
			  $( ".simpleCart_quantity" )
			  .animate({
			    fontSize: "1.2em",
			    top: "-3px"
			  }, 300 )
			  .animate({
			    fontSize: "1em",
			    top: "0px"
			  }, 300 );	
			});
